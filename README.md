# Dual Button Games

This repository contain the files required to run a game on a MicroPython ESP32
connected to a 64x48 black and white OLED display, along with two buttons.

## Usage

1. Load all the .py files onto your device
2. Also grab and put onto your device:
    - `ssd1306.py`
    - `d1_mini.py`
3. Restart the device and play away!

## Credits

Credit to Seon "Unexpected Maker" for his tiny-snake game, which has been
stripped down for use on a smaller screen with no buzzer.

