#import bbb
#import snake

from machine import I2C, Pin
import time
import ssd1306

import d1_mini

# Configuration
DEBOUNCE = 1000  # us

# Hardware setup
left = Pin(d1_mini.D3, Pin.IN, Pin.PULL_UP)
right = Pin(d1_mini.D7, Pin.IN, Pin.PULL_UP)

i2c = I2C(-1, scl=Pin(d1_mini.SCL), sda=Pin(d1_mini.SDA))
WIDTH, HEIGHT = 64, 48
oled = ssd1306.SSD1306_I2C(64, 48, i2c)

oled.fill_rect(0, 0, 64, 24, 0)
oled.fill_rect(0, 24, 64, 24, 1)
oled.text("< Snake", 0, 7, 1)
oled.text("BBB >", 20, 31, 0)
oled.show()

game = None
while not game:
    if not left.value():
        game = "snake"
    elif not right.value():
        game = "bbb"

    time.sleep_us(DEBOUNCE)

if game == "snake":
    import snake
elif game == "bbb":
    import bbb
else:
    print("Invalid game!")
